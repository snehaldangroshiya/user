all:run

build:
	$(MAKE) -C app build

run:build start-proxy
	 $(MAKE) -C app run

start-proxy:
	$(MAKE) -C sidecar start

stop:
	$(MAKE) -C sidecar stop
