package config

import (
	"net"
	"net/http"

	"github.com/sirupsen/logrus"
)

// Config struct
type Config struct {
	ListenAddr       string
	Logger           logrus.FieldLogger
	HTTPTransport    http.RoundTripper
	TrustedProxyIPs  []*net.IP
	TrustedProxyNets []*net.IPNet
}
