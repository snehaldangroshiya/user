package store

import (
	"context"
)

// Layered sturct
type Layered struct {
	ctx           context.Context
	storeSupplier *Supplier
}

// NewLayered function create the new instance of layered store.
func NewLayered(options ...Option) Store {
	layered := &Layered{
		ctx:           context.TODO(),
		storeSupplier: NewSupplier(),
	}

	for _, option := range options {
		option(layered)
	}

	return layered
}

// Stock function returns the StockStore.
func (s *Layered) Stock() StockStore {
	return s.storeSupplier.Stock()
}

// User function returns the IMAPIUser.
func (s *Layered) User() IMAPIUser {
	return s.storeSupplier.User()
}

// Contacts function returns the IMAPIContact.
func (s *Layered) Contacts() IMAPIContact {
	return s.storeSupplier.Contact()
}