package store

import "github.com/snehal1112/transport/client"

// SupplierStores struct
type SupplierStores struct {
	stockStore StockStore
	userStore IMAPIUser
	contactStore IMAPIContact
}

// Supplier struct
type Supplier struct {
	stores    SupplierStores
	transport config
}

// NewSupplier create new instance of Supplier.
func NewSupplier() *Supplier {
	supplier := &Supplier{}

	supplier.stores.stockStore = NewStockStore(supplier)
	supplier.stores.userStore = NewUserStore(supplier)
	supplier.stores.contactStore = NewContactStore(supplier)

	return supplier
}

// Stock function returns the user store object.
func (s *Supplier) Stock() StockStore {
	return s.stores.stockStore
}

// User function returns the user store object.
func (s *Supplier) User() IMAPIUser {
	return s.stores.userStore
}

// Contact function returns the user store object.
func (s *Supplier) Contact() IMAPIContact {
	return s.stores.contactStore
}

func (s *Supplier) connect() *client.Connect {
	return s.transport.connect()
}
