package store

import (
	"context"

	"github.com/snehal1112/transport/client"
)

// TODO: create separate module for this.

// transport interface
type transport interface {
	connect() *client.Connect
}

// config struct
type config struct {
	BackendEndpoint string
	DatabaseName    string
}

func (c *config) connect() *client.Connect {
	return client.NewConnection(
		client.WithDatabase(c.DatabaseName),
		client.WithCtx(context.TODO()),
		client.WithURL(c.BackendEndpoint),
		client.WithLogLevel("error"),
	)
}
