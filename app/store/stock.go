package store

import (
	"net/http"

	"github.com/snehal1112/stock/model"
)

// Stock struct
type Stock struct {
	transport
}

// NewStockStore func
func NewStockStore(transport transport) StockStore {
	return &Stock{transport}
}

// Create function is create the item in given collection.
func (s *Stock) Create(collection string, props model.SPropValue) Channel {
	storeChannel := make(Channel, 1)
	go func() {
		result := Result{}

		c := s.connect()
		err := c.CreateDocument(collection, props.ToJSON())
		if err != nil {
			result.Err = model.NewAppError("DBItemStore.Save", "store.db_item.save.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		}
		result.Data = props
		storeChannel <- result
		close(storeChannel)
	}()

	return storeChannel
}
