package store

import (
	"net/http"

	"github.com/snehal1112/stock/model"
)

// User struct
type User struct {
	transport
}

// NewUserStore func
func NewUserStore(transport transport) IMAPIUser {
	return &User{transport}
}

func (u *User) List() {
}

func (u *User) Create(collection string, user *model.User) Channel {
	storeChannel := make(Channel, 1)
	go func() {
		result := Result{}
		c := u.connect()

		user.PreSave()
		p, err := user.ToBson(user)
		if err != nil {
			result.Err = model.NewAppError("DBItemStore.Save", "store.db_item.save.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
		} else {
			err := c.CreateDocument(collection, p)
			if err != nil {
				result.Err = model.NewAppError("DBItemStore.Save", "store.db_item.save.app_error", nil, "id="+err.Error(), http.StatusBadRequest)
			}
		}
		result.Data = user
		storeChannel <- result
		close(storeChannel)
	}()
	return storeChannel
}

func (u *User) Get() {
	panic("implement me")
}

func (u *User) Update() {
	panic("implement me")
}

func (u *User) Delete() {
	panic("implement me")
}
