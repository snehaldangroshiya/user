package store

import (
	"github.com/snehal1112/stock/model"
)

// Result struct
type Result struct {
	Data interface{}
	Err  *model.AppError
}

// Channel type
type Channel chan Result

// Store interface
type Store interface {
	Stock() StockStore
	User() IMAPIUser
	Contacts() IMAPIContact
}

// StockStore interface
type StockStore interface {
	Create(collection string, props model.SPropValue) Channel
}

type IMAPIUser interface {
	List()
	Create(collection string, props *model.User) Channel
	Get()
	Update()
	Delete()
}

type IMAPIContact interface {
	Create(collection string, contactProps *model.Contact) Channel
	GetContact(collection string, id string)Channel
	CreateFolder()Channel
	Update()Channel
	Delete(collection string, id string)Channel
	List(collection string)Channel
	ListFolder()Channel
}

type IMAPIExtension interface {
	Create()
	Get()
	Update()
	Delete()
}
