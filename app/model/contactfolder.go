package model

type ContactFolder struct {
	DisplayName    string `json:"displayName"`
	ID             string `json:"id"`
	ParentFolderID string `json:"parentFolderId"`
}