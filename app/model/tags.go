package model

import (
	"encoding/json"
	"io"
	"log"

	"github.com/spf13/cast"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//noinspection GoUnusedConst
const (
	PtUnspecified = 0x0000
	PtNull        = 0x0001
	PtShort       = 0x0002
	PtLong        = 0x0003
	PtFloat       = 0x0004
	PtDouble      = 0x0005
	PtCurrency    = 0x0006
	PtApptime     = 0x0007
	PtBoolean     = 0x000B
	PtObject      = 0x000D
	PtString8     = 0x001E
	PtString      = PtUnicode
	PtUnicode     = 0x001F
	PtBinary      = 0x0102
	PtSystime     = 0x0040
	PtClsid       = 0x0048
	PtI8          = 0x0014

	mvFlag       = 0x1000
	PtMVUnicode  = mvFlag | PtUnicode
	PtMvShort    = mvFlag | PtShort
	PtMvLong     = mvFlag | PtLong
	PtMvFloat    = mvFlag | PtFloat
	PtMvDouble   = mvFlag | PtDouble
	PtMvCurrency = mvFlag | PtCurrency
	PtMvApptime  = mvFlag | PtApptime
	PtMvSystime  = mvFlag | PtSystime
	PtMvString8  = mvFlag | PtString8
	PtMvBinary   = mvFlag | PtBinary
	PtMvUnicode  = mvFlag | PtUnicode
	PtMvClsid    = mvFlag | PtClsid
	PtMvLonglong = mvFlag | PtLong
)

// PropTypeMask const
const PropTypeMask = 0x0000FFFF

// PropType func
func PropType(ulPropTag int) int {
	return ulPropTag & PropTypeMask
}

// PropTag func
func PropTag(propType, ID int) int {
	return (ID << 16) | propType
}

// PropID func
func PropID(x int) int {
	return x >> 16
}

// Props tags
var (
	PrAccount                          = PropTag(PtString, 0x3A00)
	PrDeleteAfterSubmit                = PropTag(PtBoolean, 0x0E01)
	PrNumber                           = PropTag(PtLong, 0x3307)
	PrCurrentVersion                   = PropTag(PtI8, 0x0E00)
	PrSubject                          = PropTag(PtString, 0x0037)
	PrDisplayBcc                       = PropTag(PtString, 0x0E02)
	PrDisplayCc                        = PropTag(PtString, 0x0E03)
	PrDisplayTo                        = PropTag(PtString, 0x0E04)
	PrParentDisplay                    = PropTag(PtString, 0x0E05)
	PrMessageDeliveryTime              = PropTag(PtSystime, 0x0E06)
	PrMessageFlags                     = PropTag(PtLong, 0x0E07)
	PrMessageSize                      = PropTag(PtLong, 0x0E08)
	PrParentEntryID                    = PropTag(PtBinary, 0x0E09)
	PrSentMailEntryID                  = PropTag(PtBinary, 0x0E0A)
	PrCorrelate                        = PropTag(PtBoolean, 0x0E0C)
	PrCorrelateMtsID                   = PropTag(PtBinary, 0x0E0D)
	PrDiscreteValues                   = PropTag(PtBoolean, 0x0E0E)
	PrResponsibility                   = PropTag(PtBoolean, 0x0E0F)
	PrSpoolerStatus                    = PropTag(PtLong, 0x0E10)
	PrTransportStatus                  = PropTag(PtLong, 0x0E11)
	PrMessageRecipients                = PropTag(PtObject, 0x0E12)
	PrMessageAttachments               = PropTag(PtObject, 0x0E13)
	PrSubmitFlags                      = PropTag(PtLong, 0x0E14)
	PrRecipientStatus                  = PropTag(PtLong, 0x0E15)
	PrTransportKey                     = PropTag(PtLong, 0x0E16)
	PrMsgStatus                        = PropTag(PtLong, 0x0E17)
	PrMessageDownloadTime              = PropTag(PtLong, 0x0E18)
	PrCreationVersion                  = PropTag(PtI8, 0x0E19)
	PrModifyVersion                    = PropTag(PtI8, 0x0E1A)
	PrHasAttach                        = PropTag(PtBoolean, 0x0E1B)
	PrBodyCrc                          = PropTag(PtLong, 0x0E1C)
	PrNormalizedSubject                = PropTag(PtString, 0x0E1D)
	PrRtfInSync                        = PropTag(PtBoolean, 0x0E1F)
	PrAttachSize                       = PropTag(PtLong, 0x0E20)
	PrAttachNum                        = PropTag(PtLong, 0x0E21)
	PrPreprocess                       = PropTag(PtBoolean, 0x0E22)
	PrOriginatingMtaCertificate        = PropTag(PtBinary, 0x0E25)
	PrProofOfSubmission                = PropTag(PtBinary, 0x0E26)
	PrEntryID                          = PropTag(PtBinary, 0x0FFF)
	PrObjectType                       = PropTag(PtLong, 0x0FFE)
	PrIcon                             = PropTag(PtBinary, 0x0FFD)
	PrMiniIcon                         = PropTag(PtBinary, 0x0FFC)
	PrStoreEntryID                     = PropTag(PtBinary, 0x0FFB)
	PrStoreRecordKey                   = PropTag(PtBinary, 0x0FFA)
	PrRecordKey                        = PropTag(PtBinary, 0x0FF9)
	PrMappingSignature                 = PropTag(PtBinary, 0x0FF8)
	PrAccessLevel                      = PropTag(PtLong, 0x0FF7)
	PrInstanceKey                      = PropTag(PtBinary, 0x0FF6)
	PrRowType                          = PropTag(PtLong, 0x0FF5)
	PrAccess                           = PropTag(PtLong, 0x0FF4)
	PrDisplayName                      = PropTag(PtString, 0x3001)
	PrRowid                            = PropTag(PtLong, 0x3000)
	PrAddrtype                         = PropTag(PtString, 0x3002)
	PrEmailAddress                     = PropTag(PtString, 0x3003)
	PrComment                          = PropTag(PtString, 0x3004)
	PrDepth                            = PropTag(PtLong, 0x3005)
	PrProviderDisplay                  = PropTag(PtString, 0x3006)
	PrCreationTime                     = PropTag(PtSystime, 0x3007)
	PrLastModificationTime             = PropTag(PtSystime, 0x3008)
	PrResourceFlags                    = PropTag(PtLong, 0x3009)
	PrProviderDllName                  = PropTag(PtString, 0x300A)
	PrSearchKey                        = PropTag(PtBinary, 0x300B)
	PrProviderGUID                     = PropTag(PtBinary, 0x300C)
	PrProviderOrdinal                  = PropTag(PtLong, 0x300D)
	PrSortPosition                     = PropTag(PtBinary, 0x3020)
	PrSortParentid                     = PropTag(PtBinary, 0x3021)
	PrFormVersion                      = PropTag(PtString, 0x3301) // MAPI Form properties
	PrFormClsid                        = PropTag(PtClsid, 0x3302)
	PrFormContactName                  = PropTag(PtString, 0x3303)
	PrFormCategory                     = PropTag(PtString, 0x3304)
	PrFormCategorySub                  = PropTag(PtString, 0x3305)
	PrFormHostMap                      = PropTag(PtLong, 0x3306)
	PrFormHidden                       = PropTag(PtBoolean, 0x3307)
	PrFormDesignerName                 = PropTag(PtString, 0x3308)
	PrFormDesignerGUID                 = PropTag(PtClsid, 0x3309)
	PrFormMessageBehavior              = PropTag(PtLong, 0x330A)
	PrDefaultStore                     = PropTag(PtBoolean, 0x3400) // Message store properties
	PrStoreSupportMask                 = PropTag(PtLong, 0x340D)
	PrStoreState                       = PropTag(PtLong, 0x340E)
	PrIpmSubtreeSearchKey              = PropTag(PtBinary, 0x3410)
	PrIpmOutboxSearchKey               = PropTag(PtBinary, 0x3411)
	PrIpmWastebasketSearchKey          = PropTag(PtBinary, 0x3412)
	PrIpmSentmailSearchKey             = PropTag(PtBinary, 0x3413)
	PrMdbProvider                      = PropTag(PtBinary, 0x3414)
	PrReceiveFolderSettings            = PropTag(PtObject, 0x3415)
	PrValidFolderMask                  = PropTag(PtLong, 0x35DF)
	PrIpmSubtreeEntryid                = PropTag(PtBinary, 0x35E0)
	PrIpmOutboxEntryid                 = PropTag(PtBinary, 0x35E2)
	PrIpmWastebasketEntryid            = PropTag(PtBinary, 0x35E3)
	PrIpmSentmailEntryid               = PropTag(PtBinary, 0x35E4)
	PrViewsEntryid                     = PropTag(PtBinary, 0x35E5)
	PrCommonViewsEntryid               = PropTag(PtBinary, 0x35E6)
	PrFinderEntryid                    = PropTag(PtBinary, 0x35E7)
	PrContainerFlags                   = PropTag(PtLong, 0x3600) // Folder and AB Container properties
	PrFolderType                       = PropTag(PtLong, 0x3601)
	PrContentCount                     = PropTag(PtLong, 0x3602)
	PrContentUnread                    = PropTag(PtLong, 0x3603)
	PrCreateTemplates                  = PropTag(PtObject, 0x3604)
	PrDetailsTable                     = PropTag(PtObject, 0x3605)
	PrSearch                           = PropTag(PtObject, 0x3607)
	PrSelectable                       = PropTag(PtBoolean, 0x3609)
	PrSubfolders                       = PropTag(PtBoolean, 0x360A)
	PrStatus                           = PropTag(PtLong, 0x360B)
	PrAnr                              = PropTag(PtString, 0x360C)
	PrContentsSortOrder                = PropTag(PtLong, 0x360D)
	PrContainerHierarchy               = PropTag(PtObject, 0x360E)
	PrContainerContents                = PropTag(PtObject, 0x360F)
	PrFolderAssociatedContents         = PropTag(PtObject, 0x3610)
	PrDefCreateDl                      = PropTag(PtBinary, 0x3611)
	PrDefCreateMailuser                = PropTag(PtBinary, 0x3612)
	PrContainerClass                   = PropTag(PtString, 0x3613)
	PrContainerModifyVersion           = PropTag(PtI8, 0x3614)
	PrAbProviderID                     = PropTag(PtBinary, 0x3615)
	PrDefaultViewEntryid               = PropTag(PtBinary, 0x3616)
	PrAssocContentCount                = PropTag(PtLong, 0x3617)
	PrAttachmentX400Parameters         = PropTag(PtBinary, 0x3700) // Attachment properties
	PrAttachDataObj                    = PropTag(PtObject, 0x3701)
	PrAttachDataBin                    = PropTag(PtBinary, 0x3701)
	PrAttachEncoding                   = PropTag(PtBinary, 0x3702)
	PrAttachExtension                  = PropTag(PtString, 0x3703)
	PrAttachFilename                   = PropTag(PtString, 0x3704)
	PrAttachMethod                     = PropTag(PtLong, 0x3705)
	PrAttachLongFilename               = PropTag(PtString, 0x3707)
	PrAttachPathname                   = PropTag(PtString, 0x3708)
	PrAttachRendering                  = PropTag(PtBinary, 0x3709)
	PrAttachTag                        = PropTag(PtBinary, 0x370A)
	PrRenderingPosition                = PropTag(PtLong, 0x370B)
	PrAttachTransportName              = PropTag(PtString, 0x370C)
	PrAttachLongPathname               = PropTag(PtString, 0x370D)
	PrAttachMimeTag                    = PropTag(PtString, 0x370E)
	PrAttachAdditionalInfo             = PropTag(PtBinary, 0x370F)
	PrDisplayType                      = PropTag(PtLong, 0x3900) // AB Object properties
	PrTemplateID                       = PropTag(PtBinary, 0x3902)
	PrPrimaryCapability                = PropTag(PtBinary, 0x3904)
	Pr7bitDisplayName                  = PropTag(PtString8, 0x39FF) // Mail user properties
	PrAlternateRecipient               = PropTag(PtBinary, 0x3A01)
	PrCallbackTelephoneNumber          = PropTag(PtString, 0x3A02)
	PrConversionProhibited             = PropTag(PtBoolean, 0x3A03)
	PrDiscloseRecipients               = PropTag(PtBoolean, 0x3A04)
	PrGeneration                       = PropTag(PtString, 0x3A05)
	PrGivenName                        = PropTag(PtString, 0x3A06)
	PrGovernmentIDNumber               = PropTag(PtString, 0x3A07)
	PrBusinessTelephoneNumber          = PropTag(PtString, 0x3A08)
	PrOfficeTelephoneNumber            = PrBusinessTelephoneNumber
	PrHomeTelephoneNumber              = PropTag(PtString, 0x3A09)
	PrInitials                         = PropTag(PtString, 0x3A0A)
	PrKeyword                          = PropTag(PtString, 0x3A0B)
	PrLanguage                         = PropTag(PtString, 0x3A0C)
	PrLocation                         = PropTag(PtString, 0x3A0D)
	PrMailPermission                   = PropTag(PtBoolean, 0x3A0E)
	PrMhsCommonName                    = PropTag(PtString, 0x3A0F)
	PrOrganizationalIDNumber           = PropTag(PtString, 0x3A10)
	PrSurname                          = PropTag(PtString, 0x3A11)
	PrOriginalEntryid                  = PropTag(PtBinary, 0x3A12)
	PrOriginalDisplayName              = PropTag(PtString, 0x3A13)
	PrOriginalSearchKey                = PropTag(PtBinary, 0x3A14)
	PrPostalAddress                    = PropTag(PtString, 0x3A15)
	PrCompanyName                      = PropTag(PtString, 0x3A16)
	PrTitle                            = PropTag(PtString, 0x3A17)
	PrDepartmentName                   = PropTag(PtString, 0x3A18)
	PrOfficeLocation                   = PropTag(PtString, 0x3A19)
	PrPrimaryTelephoneNumber           = PropTag(PtString, 0x3A1A)
	PrBusiness2TelephoneNumber         = PropTag(PtString, 0x3A1B)
	PrOffice2TelephoneNumber           = PrBusiness2TelephoneNumber
	PrMobileTelephoneNumber            = PropTag(PtString, 0x3A1C)
	PrCellularTelephoneNumber          = PrMobileTelephoneNumber
	PrRadioTelephoneNumber             = PropTag(PtString, 0x3A1D)
	PrCarTelephoneNumber               = PropTag(PtString, 0x3A1E)
	PrOtherTelephoneNumber             = PropTag(PtString, 0x3A1F)
	PrTransmitableDisplayName          = PropTag(PtString, 0x3A20)
	PrPagerTelephoneNumber             = PropTag(PtString, 0x3A21)
	PrBeeperTelephoneNumber            = PrPagerTelephoneNumber
	PrUserCertificate                  = PropTag(PtBinary, 0x3A22)
	PrPrimaryFaxNumber                 = PropTag(PtString, 0x3A23)
	PrBusinessFaxNumber                = PropTag(PtString, 0x3A24)
	PrHomeFaxNumber                    = PropTag(PtString, 0x3A25)
	PrCountry                          = PropTag(PtString, 0x3A26)
	PrBusinessAddressCountry           = PrCountry
	PrLocality                         = PropTag(PtString, 0x3A27)
	PrBusinessAddressCity              = PrLocality
	PrStateOrProvince                  = PropTag(PtString, 0x3A28)
	PrBusinessAddressStateOrProvince   = PrStateOrProvince
	PrStreetAddress                    = PropTag(PtString, 0x3A29)
	PrBusinessAddressStreet            = PrStreetAddress
	PrPostalCode                       = PropTag(PtString, 0x3A2A)
	PrBusinessAddressPostalCode        = PrPostalCode
	PrPostOfficeBox                    = PropTag(PtString, 0x3A2B)
	PrBusinessAddressPostOfficeBox     = PrPostOfficeBox
	PrTelexNumber                      = PropTag(PtString, 0x3A2C)
	PrISDNNumber                       = PropTag(PtString, 0x3A2D)
	PrAssistantTelephoneNumber         = PropTag(PtString, 0x3A2E)
	PrHome2TelephoneNumber             = PropTag(PtString, 0x3A2F)
	PrAssistant                        = PropTag(PtString, 0x3A30)
	PrSendRichInfo                     = PropTag(PtBoolean, 0x3A40)
	PrWeddingAnniversary               = PropTag(PtSystime, 0x3A41)
	PrBirthday                         = PropTag(PtSystime, 0x3A42)
	PrHobbies                          = PropTag(PtString, 0x3A43)
	PrMiddleName                       = PropTag(PtString, 0x3A44)
	PrDisplayNamePrefix                = PropTag(PtString, 0x3A45)
	PrProfession                       = PropTag(PtString, 0x3A46)
	PrPreferredByName                  = PropTag(PtString, 0x3A47)
	PrSpouseName                       = PropTag(PtString, 0x3A48)
	PrComputerNetworkName              = PropTag(PtString, 0x3A49)
	PrCustomerID                       = PropTag(PtString, 0x3A4A)
	PrTTYtddPhoneNumber                = PropTag(PtString, 0x3A4B)
	PrFTPSite                          = PropTag(PtString, 0x3A4C)
	PrGender                           = PropTag(PtShort, 0x3A4D)
	PrManagerName                      = PropTag(PtString, 0x3A4E)
	PrNickname                         = PropTag(PtString, 0x3A4F)
	PrPersonalHomePage                 = PropTag(PtString, 0x3A50)
	PrBusinessHomePage                 = PropTag(PtString, 0x3A51)
	PrContactVersion                   = PropTag(PtClsid, 0x3A52)
	PrContactEntryIDs                  = PropTag(PtBinary, 0x3A53)
	PrContactAddrtypes                 = PropTag(PtSystime, 0x3A54)
	PrContactDefaultAddressIndex       = PropTag(PtLong, 0x3A55)
	PrContactEmailAddresses            = PropTag(PtString, 0x3A56)
	PrCompanyMainPhoneNumber           = PropTag(PtString, 0x3A57)
	PrChildrensNames                   = PropTag(PtString, 0x3A58)
	PrHomeAddressCity                  = PropTag(PtString, 0x3A59)
	PrHomeAddressCountry               = PropTag(PtString, 0x3A5A)
	PrHomeAddressPostalCode            = PropTag(PtString, 0x3A5B)
	PrHomeAddressStateOrProvince       = PropTag(PtString, 0x3A5C)
	PrHomeAddressStreet                = PropTag(PtString, 0x3A5D)
	PrHomeAddressPostOfficeBox         = PropTag(PtString, 0x3A5E)
	PrOtherAddressCity                 = PropTag(PtString, 0x3A5F)
	PrOtherAddressCountry              = PropTag(PtString, 0x3A60)
	PrOtherAddressPostalCode           = PropTag(PtString, 0x3A61)
	PrOtherAddressStateOrProvince      = PropTag(PtString, 0x3A62)
	PrOtherAddressStreet               = PropTag(PtString, 0x3A63)
	PrOtherAddressPostOfficeBox        = PropTag(PtString, 0x3A64)
	PrStoreProviders                   = PropTag(PtBinary, 0x3D00) // Profile section properties
	PrABProviders                      = PropTag(PtBinary, 0x3D01)
	PrTransportProviders               = PropTag(PtBinary, 0x3D02)
	PrDefaultProfile                   = PropTag(PtBoolean, 0x3D04)
	PrABSearchPath                     = PropTag(PtBinary, 0x3D05)
	PrABDefaultDir                     = PropTag(PtBinary, 0x3D06)
	PrABDefaultPab                     = PropTag(PtBinary, 0x3D07)
	PrFilteringHooks                   = PropTag(PtBinary, 0x3D08)
	PrServiceName                      = PropTag(PtString, 0x3D09)
	PrServiceEntryName                 = PropTag(PtString8, 0x3D0B)
	PrServiceUID                       = PropTag(PtBinary, 0x3D0C)
	PrServiceExtraUIDs                 = PropTag(PtBinary, 0x3D0D)
	PrServices                         = PropTag(PtBinary, 0x3D0E)
	PrServiceSupportFiles              = PropTag(PtString8, 0x3D0F)
	PrServiceDeleteFiles               = PropTag(PtMvString8, 0x3D10)
	PrAbSearchPathUpdate               = PropTag(PtBinary, 0x3D11)
	PrProfileName                      = PropTag(PtString, 0x3D12)
	PrIdentityDisplay                  = PropTag(PtString, 0x3E00) // Status object properties
	PrIdentityEntryID                  = PropTag(PtBinary, 0x3E01)
	PrResourceMethods                  = PropTag(PtLong, 0x3E02)
	PrResourceType                     = PropTag(PtString, 0x3E03)
	PrStatusCode                       = PropTag(PtLong, 0x3E04)
	PrIdentitySearchKey                = PropTag(PtBinary, 0x3E05)
	PrOwnStoreEntryID                  = PropTag(PtBinary, 0x3E06)
	PrResourcePath                     = PropTag(PtString, 0x3E07)
	PrStatusString                     = PropTag(PtString, 0x3E08)
	PrX400DeferredDeliveryCancel       = PropTag(PtBoolean, 0x3E09)
	PrHeaderFolderEntryid              = PropTag(PtBinary, 0x3E0A)
	PrRemoteProgress                   = PropTag(PtLong, 0x3E0B)
	PrRemoteProgressText               = PropTag(PtString, 0x3E0C)
	PrRemoteValidateOk                 = PropTag(PtBoolean, 0x3E0D)
	PrControlFlags                     = PropTag(PtLong, 0x3F00) // Display table properties
	PrControlStructure                 = PropTag(PtBinary, 0x3F01)
	PrControlType                      = PropTag(PtLong, 0x3F02)
	PrDeltaX                           = PropTag(PtLong, 0x3F03)
	PrDeltaY                           = PropTag(PtLong, 0x3F04)
	PrXPos                             = PropTag(PtLong, 0x3F05)
	PrYPos                             = PropTag(PtLong, 0x3F06)
	PrControlID                        = PropTag(PtBinary, 0x3F07)
	PrInitialDetailsPane               = PropTag(PtLong, 0x3F08)
	PropIDSecureMin                    = 0x67F0 // Secure property id range
	PropIDSecureMax                    = 0x67FF
	PrNonIpmSubtreeEntryid             = PropTag(PtBinary, 0x6620)
	PrIpmFavoritesEntryid              = PropTag(PtBinary, 0x6630)
	PrIpmPublicFoldersEntryid          = PropTag(PtBinary, 0x6631)
	PrProfileVersion                   = PropTag(PtLong, 0x6600)
	PrProfileConfigFlags               = PropTag(PtLong, 0x6601)
	PrProfileHomeServer                = PropTag(PtString8, 0x6602)
	PrProfileHomeServerDn              = PropTag(PtString8, 0x6612)
	PrProfileHomeServerAddrs           = PropTag(PtMvString8, 0x6613)
	PrProfileUser                      = PropTag(PtString8, 0x6603)
	PrProfileConnectFlags              = PropTag(PtLong, 0x6604)
	PrProfileTransportFlags            = PropTag(PtLong, 0x6605)
	PrProfileUIState                   = PropTag(PtLong, 0x6606)
	PrProfileUnresolvedName            = PropTag(PtString8, 0x6607)
	PrProfileUnresolvedServer          = PropTag(PtString8, 0x6608)
	PrProfileBindingOrder              = PropTag(PtString8, 0x6609)
	PrProfileMaxRestrict               = PropTag(PtLong, 0x660D)
	PrProfileAbFilesPath               = PropTag(PtString8, 0x660E)
	PrProfileOfflineStorePath          = PropTag(PtString8, 0x6610)
	PrProfileOfflineInfo               = PropTag(PtBinary, 0x6611)
	PrProfileAddrInfo                  = PropTag(PtBinary, 0x6687)
	PrProfileOptionsData               = PropTag(PtBinary, 0x6689)
	PrProfileSecureMailbox             = PropTag(PtBinary, 0x67F0)
	PrDisableWinsock                   = PropTag(PtLong, 0x6618)
	PrProfileAuthPackage               = PropTag(PtLong, 0x6619)
	PrProfileReconnectInterval         = PropTag(PtLong, 0x661A)
	PrProfileServerVersion             = PropTag(PtLong, 0x661B)
	PrOstEncryption                    = PropTag(PtLong, 0x6702)
	PrProfileOpenFlags                 = PropTag(PtLong, 0x6609)
	PrProfileType                      = PropTag(PtLong, 0x660A)
	PrProfileMailbox                   = PropTag(PtString8, 0x660B)
	PrProfileServer                    = PropTag(PtString8, 0x660C)
	PrProfileServerDn                  = PropTag(PtString8, 0x6614)
	PrProfileFavfldDisplayName         = PropTag(PtString8, 0x660F)
	PrProfileFavfldComment             = PropTag(PtString8, 0x6615)
	PrProfileAllpubDisplayName         = PropTag(PtString8, 0x6616)
	PrProfileAllpubComment             = PropTag(PtString8, 0x6617)
	PrProfileMoab                      = PropTag(PtString8, 0x667B)
	PrProfileMoabGUID                  = PropTag(PtString8, 0x667C)
	PrProfileMoabSeq                   = PropTag(PtLong, 0x667D)
	PrGetPropsExcludePropIDList        = PropTag(PtBinary, 0x667E)
	PrUserEntryid                      = PropTag(PtBinary, 0x6619)
	PrUserName                         = PropTag(PtString, 0x661A)
	PrMailboxOwnerEntryid              = PropTag(PtBinary, 0x661B)
	PrMailboxOwnerName                 = PropTag(PtString, 0x661C)
	PrOofState                         = PropTag(PtBoolean, 0x661D)
	PrHierarchyServer                  = PropTag(PtString, 0x6633)
	PrScheduleFolderEntryID            = PropTag(PtBinary, 0x661E)
	PrIpmDafEntryID                    = PropTag(PtBinary, 0x661F)
	PrEformsRegistryEntryID            = PropTag(PtBinary, 0x6621)
	PrSplusFreeBusyEntryID             = PropTag(PtBinary, 0x6622)
	PrOfflineAddressBookEntryID        = PropTag(PtBinary, 0x6623)
	PrNntpControlFolderEntryID         = PropTag(PtBinary, 0x668B)
	PrEformsForLocaleEntryID           = PropTag(PtBinary, 0x6624)
	PrFreeBusyForLocalSiteEntryID      = PropTag(PtBinary, 0x6625)
	PrAddressBookForLocalSiteEntryID   = PropTag(PtBinary, 0x6626)
	PrNewsgroupRootFolderEntryid       = PropTag(PtBinary, 0x668C)
	PrOfflineMessageEntryid            = PropTag(PtBinary, 0x6627)
	PrFavoritesDefaultName             = PropTag(PtString8, 0x6635)
	PrSysConfigFolderEntryid           = PropTag(PtBinary, 0x6636)
	PrNntpArticleFolderEntryid         = PropTag(PtBinary, 0x668A)
	PrEventsRootFolderEntryid          = PropTag(PtBinary, 0x667A)
	PrGwMtsinEntryid                   = PropTag(PtBinary, 0x6628)
	PrGwMtsoutEntryid                  = PropTag(PtBinary, 0x6629)
	PrTransferEnabled                  = PropTag(PtBoolean, 0x662A)
	PrTestLineSpeed                    = PropTag(PtBinary, 0x662B)
	PrHierarchySynchronizer            = PropTag(PtObject, 0x662C)
	PrContentsSynchronizer             = PropTag(PtObject, 0x662D)
	PrCollector                        = PropTag(PtObject, 0x662E)
	PrFastTransfer                     = PropTag(PtObject, 0x662F)
	PrChangeAdvisor                    = PropTag(PtObject, 0x6634)
	PrChangeNotificationGUID           = PropTag(PtClsid, 0x6637)
	PrStoreOffline                     = PropTag(PtBoolean, 0x6632)
	PrInTransit                        = PropTag(PtBoolean, 0x6618)
	PrReplicationStyle                 = PropTag(PtLong, 0x6690)
	PrReplicationSchedule              = PropTag(PtBinary, 0x6691)
	PrReplicationMessagePriority       = PropTag(PtLong, 0x6692)
	PrOverallMsgAgeLimit               = PropTag(PtLong, 0x6693)
	PrReplicationAlwaysInterval        = PropTag(PtLong, 0x6694)
	PrReplicationMsgSize               = PropTag(PtLong, 0x6695)
	PrSourceKey                        = PropTag(PtBinary, 0x65E0)
	PrParentSourceKey                  = PropTag(PtBinary, 0x65E1)
	PrChangeKey                        = PropTag(PtBinary, 0x65E2)
	PrPredecessorChangeList            = PropTag(PtBinary, 0x65E3)
	PrSourceFid                        = PropTag(PtI8, 0x0E5F)
	PrCatalog                          = PropTag(PtBinary, 0x0E5B)
	PrCiSearchEnabled                  = PropTag(PtBoolean, 0x0E5C)
	PrCiNotificationEnabled            = PropTag(PtBoolean, 0x0E5D)
	PrMaxCachedViews                   = PropTag(PtLong, 0x0E68)
	PrMaxIndices                       = PropTag(PtLong, 0x0E5E)
	PrImpliedRestrictions              = PropTag(PtMvBinary, 0x667F)
	PrFolderChildCount                 = PropTag(PtLong, 0x6638)
	PrRights                           = PropTag(PtLong, 0x6639)
	PrACLTable                         = PropTag(PtObject, 0x3FE0)
	PrRulesTable                       = PropTag(PtObject, 0x3FE1)
	PrHasRules                         = PropTag(PtBoolean, 0x663A)
	PrHasModeratorRules                = PropTag(PtBoolean, 0x663F)
	PrAddressBookEntryid               = PropTag(PtBinary, 0x663B)
	PrACLData                          = PropTag(PtBinary, 0x3FE0)
	PrRulesData                        = PropTag(PtBinary, 0x3FE1)
	PrExtendedACLData                  = PropTag(PtBinary, 0x3FFE)
	PrFolderDesignFlags                = PropTag(PtLong, 0x3FE2)
	PrDesignInProgress                 = PropTag(PtBoolean, 0x3FE4)
	PrSecureOrigination                = PropTag(PtBoolean, 0x3FE5)
	PrPublishInAddressBook             = PropTag(PtBoolean, 0x3FE6)
	PrResolveMethod                    = PropTag(PtLong, 0x3FE7)
	PrAddressBookDisplayName           = PropTag(PtString, 0x3FE8)
	PrEformsLocaleID                   = PropTag(PtLong, 0x3FE9)
	PrReplicaList                      = PropTag(PtBinary, 0x6698)
	PrOverallAgeLimit                  = PropTag(PtLong, 0x6699)
	PrIsNewsgroupAnchor                = PropTag(PtBoolean, 0x6696)
	PrIsNewsgroup                      = PropTag(PtBoolean, 0x6697)
	PrNewsgroupComponent               = PropTag(PtString8, 0x66A5)
	PrInternetNewsgroupName            = PropTag(PtString8, 0x66A7)
	PrNewsfeedInfo                     = PropTag(PtBinary, 0x66A6)
	PrPreventMsgCreate                 = PropTag(PtBoolean, 0x65F4)
	PrImapInternalDate                 = PropTag(PtSystime, 0x65F5)
	PrInboundNewsfeedDn                = PropTag(PtString8, 0x668D)
	PrOutboundNewsfeedDn               = PropTag(PtString8, 0x668E)
	PrInternetCharset                  = PropTag(PtString, 0x669A)
	PrPublicFolderEntryid              = PropTag(PtBinary, 0x663C)
	PrHierarchyChangeNum               = PropTag(PtLong, 0x663E)
	PrHasNamedProperties               = PropTag(PtBoolean, 0x664A)
	PrCreatorName                      = PropTag(PtString, 0x3FF8)
	PrCreatorEntryid                   = PropTag(PtBinary, 0x3FF9)
	PrLastModifierName                 = PropTag(PtString, 0x3FFA)
	PrLastModifierEntryid              = PropTag(PtBinary, 0x3FFB)
	PrReplyRecipientSMTPProxies        = PropTag(PtString, 0x3FFC)
	PrHasDams                          = PropTag(PtBoolean, 0x3FEA)
	PrRuleTriggerHistory               = PropTag(PtBinary, 0x3FF2)
	PrMoveToStoreEntryid               = PropTag(PtBinary, 0x3FF3)
	PrMoveToFolderEntryid              = PropTag(PtBinary, 0x3FF4)
	PrReplicaServer                    = PropTag(PtString, 0x6644)
	PrReplicaVersion                   = PropTag(PtI8, 0x664B)
	PrCreatorSID                       = PropTag(PtBinary, 0x0E58)
	PrLastModifierSid                  = PropTag(PtBinary, 0x0E59)
	PrSenderSID                        = PropTag(PtBinary, 0x0E4D)
	PrSentRepresentingSid              = PropTag(PtBinary, 0x0E4E)
	PrOriginalSenderSid                = PropTag(PtBinary, 0x0E4F)
	PrOriginalSentRepresentingSid      = PropTag(PtBinary, 0x0E50)
	PrReadReceiptSid                   = PropTag(PtBinary, 0x0E51)
	PrReportSid                        = PropTag(PtBinary, 0x0E52)
	PrOriginatorSid                    = PropTag(PtBinary, 0x0E53)
	PrReportDestinationSid             = PropTag(PtBinary, 0x0E54)
	PrOriginalAuthorSid                = PropTag(PtBinary, 0x0E55)
	PrReceivedBySid                    = PropTag(PtBinary, 0x0E56)
	PrRcvdRepresentingSid              = PropTag(PtBinary, 0x0E57)
	PrTrustSenderNo                    = 0x00000000
	PrTrustSenderYes                   = 0x00000001
	PrTrustSender                      = PropTag(PtLong, 0x0E79)
	PrCreatorSidAsXML                  = PropTag(PtString, 0x0E6C)
	PrLastModifierSidAsXML             = PropTag(PtString, 0x0E6D)
	PrSenderSidAsXML                   = PropTag(PtString, 0x0E6E)
	PrSentRepresentingSidAsXML         = PropTag(PtString, 0x0E6F)
	PrOriginalSenderSidAsXML           = PropTag(PtString, 0x0E70)
	PrOriginalSentRepresentingSidAsXML = PropTag(PtString, 0x0E71)
	PrReadReceiptSidAsXML              = PropTag(PtString, 0x0E72)
	PrReportSidAsXML                   = PropTag(PtString, 0x0E73)
	PrOriginatorSidAsXML               = PropTag(PtString, 0x0E74)
	PrReportDestinationSidAsXML        = PropTag(PtString, 0x0E75)
	PrOriginalAuthorSidAsXML           = PropTag(PtString, 0x0E76)
	PrReceivedBySidAsXML               = PropTag(PtString, 0x0E77)
	PrRcvdRepresentingSidAsXML         = PropTag(PtString, 0x0E78)
	PrMergeMidsetDeleted               = PropTag(PtBinary, 0x0E7A)
	PrReserveRangeOfIds                = PropTag(PtBinary, 0x0E7B)
	PrFidVid                           = PropTag(PtBinary, 0x664C)
	PrOriginID                         = PropTag(PtBinary, 0x664D)
	PrRank                             = PropTag(PtLong, 0x6712)
	PrMsgFoldTime                      = PropTag(PtSystime, 0x6654)
	PrIcsChangeKey                     = PropTag(PtBinary, 0x6655)
	PrDeferredSendNumber               = PropTag(PtLong, 0x3FEB)
	PrDeferredSendUnits                = PropTag(PtLong, 0x3FEC)
	PrExpiryNumber                     = PropTag(PtLong, 0x3FED)
	PrExpiryUnits                      = PropTag(PtLong, 0x3FEE)
	PrDeferredSendTime                 = PropTag(PtSystime, 0x3FEF)
	PrGwAdminOperations                = PropTag(PtLong, 0x6658)
	PrP1Content                        = PropTag(PtBinary, 0x1100)
	PrP1ContentType                    = PropTag(PtBinary, 0x1101)
	PrClientActions                    = PropTag(PtBinary, 0x6645)
	PrDamOriginalEntryid               = PropTag(PtBinary, 0x6646)
	PrDamBackPatched                   = PropTag(PtBoolean, 0x6647)
	PrRuleError                        = PropTag(PtLong, 0x6648)
	PrRuleActionType                   = PropTag(PtLong, 0x6649)
	PrRuleActionNumber                 = PropTag(PtLong, 0x6650)
	PrRuleFolderEntryid                = PropTag(PtBinary, 0x6651)
	PrInternetContent                  = PropTag(PtBinary, 0x6659)
	PrMimeSize                         = PropTag(PtLong, 0x6746)
	PrMimeSizeExtended                 = PropTag(PtI8, 0x6746)
	PrFileSize                         = PropTag(PtLong, 0x6747)
	PrFileSizeExtended                 = PropTag(PtI8, 0x6747)
	PrMsgEditorFormat                  = PropTag(PtLong, 0x5909)
	PrActiveUserEntryid                = PropTag(PtBinary, 0x6652)
	PrConflictEntryid                  = PropTag(PtBinary, 0x3FF0)
	PrMessageLocaleID                  = PropTag(PtLong, 0x3FF1)
	PrMessageCodepage                  = PropTag(PtLong, 0x3FFD)
	PrStorageQuotaLimit                = PropTag(PtLong, 0x3FF5)
	PrExcessStorageUsed                = PropTag(PtLong, 0x3FF6)
	PrSvrGeneratingQuotaMsg            = PropTag(PtString, 0x3FF7)
	PrDelegatedByRule                  = PropTag(PtBoolean, 0x3FE3)
	PrX400EnvelopeType                 = PropTag(PtLong, 0x6653)
	PrAutoResponseSuppress             = PropTag(PtLong, 0x3FDF)
	PrInternetCpid                     = PropTag(PtLong, 0x3FDE)
	PrSynceventFired                   = PropTag(PtBoolean, 0x664F)
	PrInConflict                       = PropTag(PtBoolean, 0x666C)
	PrDeletedOn                        = PropTag(PtSystime, 0x668F)
	PrDeletedMsgCount                  = PropTag(PtLong, 0x6640)
	PrDeletedAssocMsgCount             = PropTag(PtLong, 0x6643)
	PrDeletedFolderCount               = PropTag(PtLong, 0x6641)
	PrOldestDeletedOn                  = PropTag(PtSystime, 0x6642)
	PrDeletedMessageSizeExtended       = PropTag(PtI8, 0x669B)
	PrDeletedNormalMessageSizeExtended = PropTag(PtI8, 0x669C)
	PrDeletedAssocMessageSizeExtended  = PropTag(PtI8, 0x669D)
	PrRetentionAgeLimit                = PropTag(PtLong, 0x66C4)
	PrDisablePeruserRead               = PropTag(PtBoolean, 0x66C5)
	PrLastFullBackup                   = PropTag(PtSystime, 0x6685)
	PrURLName                          = PropTag(PtString, 0x6707)
	PrURLCompName                      = PropTag(PtString, 0x10F3)
	PrParentURLName                    = PropTag(PtString, 0x670D)
	PrFlatURLName                      = PropTag(PtString, 0x670E)
	PrSrcURLName                       = PropTag(PtString, 0x670F)
	PrSecureInSite                     = PropTag(PtBoolean, 0x669E)
	PrLocalCommitTime                  = PropTag(PtSystime, 0x6709)
	PrLocalCommitTimeMax               = PropTag(PtSystime, 0x670A)
	PrDeletedCountTotal                = PropTag(PtLong, 0x670B)
	PrAutoReset                        = PropTag(PtMvClsid, 0x670C)
	PrLongtermEntryidFromTable         = PropTag(PtBinary, 0x6670)
	PrSubfolder                        = PropTag(PtBoolean, 0x6708)
	PtSrestriction                     = 0x00FD
	PrOriginatorName                   = PropTag(PtString, 0x665B)
	PrOriginatorAddr                   = PropTag(PtString, 0x665C)
	PrOriginatorAddrtype               = PropTag(PtString, 0x665D)
	PrOriginatorEntryid                = PropTag(PtBinary, 0x665E)
	PrArrivalTime                      = PropTag(PtSystime, 0x665F)
	PrTraceInfo                        = PropTag(PtBinary, 0x6660)
	PrInternalTraceInfo                = PropTag(PtBinary, 0x666A)
	PrSubjectTraceInfo                 = PropTag(PtBinary, 0x6661)
	PrRecipientNumber                  = PropTag(PtLong, 0x6662)
	PrMtsSubjectID                     = PropTag(PtBinary, 0x6663)
	PrReportDestinationName            = PropTag(PtString, 0x6664)
	PrReportDestinationEntryid         = PropTag(PtBinary, 0x6665)
	PrContentSearchKey                 = PropTag(PtBinary, 0x6666)
	PrForeignID                        = PropTag(PtBinary, 0x6667)
	PrForeignReportID                  = PropTag(PtBinary, 0x6668)
	PrForeignSubjectID                 = PropTag(PtBinary, 0x6669)
	PrPromotePropIDList                = PropTag(PtBinary, 0x666B)
	PrMemberID                         = PropTag(PtI8, 0x6671)
	PrMemberName                       = PropTag(PtString, 0x6672)
	PrMemberEntryid                    = PrEntryID
	PrMemberRights                     = PropTag(PtLong, 0x6673)
	PrRuleID                           = PropTag(PtI8, 0x6674)
	PrRuleIds                          = PropTag(PtBinary, 0x6675)
	PrRuleSequence                     = PropTag(PtLong, 0x6676)
	PrRuleState                        = PropTag(PtLong, 0x6677)
	PrRuleUserFlags                    = PropTag(PtLong, 0x6678)
	PrRuleCondition                    = PropTag(PtSrestriction, 0x6679)
	PrRuleProvider                     = PropTag(PtString8, 0x6681)
	PrRuleName                         = PropTag(PtString, 0x6682)
	PrRuleLevel                        = PropTag(PtLong, 0x6683)
	PrRuleProviderData                 = PropTag(PtBinary, 0x6684)
	PrExtendedRuleActions              = PropTag(PtBinary, 0x0E99)
	PrExtendedRuleCondition            = PropTag(PtBinary, 0x0E9A)
	PrExtendedRuleSizeLimit            = PropTag(PtLong, 0x0E9B)
	PrNtUserName                       = PropTag(PtString, 0x66A0)
	PrLastLogonTime                    = PropTag(PtSystime, 0x66A2)
	PrLastLogoffTime                   = PropTag(PtSystime, 0x66A3)
	PrStorageLimitInformation          = PropTag(PtLong, 0x66A4)
	PrQuotaWarningThreshold            = PropTag(PtLong, 0x6721)
	PrQuotaSendThreshold               = PropTag(PtLong, 0x6722)
	PrQuotaReceiveThreshold            = PropTag(PtLong, 0x6723)
	PrFolderFlags                      = PropTag(PtLong, 0x66A8)
	PrLastAccessTime                   = PropTag(PtSystime, 0x66A9)
	PrRestrictionCount                 = PropTag(PtLong, 0x66AA)
	PrCategCount                       = PropTag(PtLong, 0x66AB)
	PrCachedColumnCount                = PropTag(PtLong, 0x66AC)
	PrNormalMsgWAttachCount            = PropTag(PtLong, 0x66AD)
	PrAssocMsgWAttachCount             = PropTag(PtLong, 0x66AE)
	PrRecipientOnNormalMsgCount        = PropTag(PtLong, 0x66AF)
	PrRecipientOnAssocMsgCount         = PropTag(PtLong, 0x66B0)
	PrAttachOnNormalMsgCount           = PropTag(PtLong, 0x66B1)
	PrAttachOnAssocMsgCount            = PropTag(PtLong, 0x66B2)
	PrNormalMessageSize                = PropTag(PtLong, 0x66B3)
	PrNormalMessageSizeExtended        = PropTag(PtI8, 0x66B3)
	PrAssocMessageSize                 = PropTag(PtLong, 0x66B4)
	PrAssocMessageSizeExtended         = PropTag(PtI8, 0x66B4)
	PrFolderPathname                   = PropTag(PtString, 0x66B5)
	PrOwnerCount                       = PropTag(PtLong, 0x66B6)
	PrContactCount                     = PropTag(PtLong, 0x66B7)
	PrPfOverHardQuotaLimit             = PropTag(PtLong, 0x6721)
	PrPfMsgSizeLimit                   = PropTag(PtLong, 0x6722)
	PrPfDisallowMdbWideExpiry          = PropTag(PtBoolean, 0x6723)
	PrLocaleID                         = PropTag(PtLong, 0x66A1)
	PrCodePageID                       = PropTag(PtLong, 0x66C3)
	PrSortLocaleID                     = PropTag(PtLong, 0x6705)
	PrAutoAddNewSubs                   = PropTag(PtBoolean, 0x65E5)
	PrNewSubsGetAutoAdd                = PropTag(PtBoolean, 0x65E6)
	PrOfflineFlags                     = PropTag(PtLong, 0x663D)
	PrSynchronizeFlags                 = PropTag(PtLong, 0x65E4)
	PrMessageSiteName                  = PropTag(PtString, 0x65E7)
	PrMessageProcessed                 = PropTag(PtBoolean, 0x65E8)
	PrMsgBodyID                        = PropTag(PtLong, 0x3FDD)
	PrBilateralInfo                    = PropTag(PtBinary, 0x3FDC)
	PrDlReportFlags                    = PropTag(PtLong, 0x3FDB)
	PrAbstract                         = PropTag(PtString, 0x3FDA)
	PrPreview                          = PropTag(PtString, 0x3FD9)
	PrPreviewUnread                    = PropTag(PtString, 0x3FD8)
	PrDisableFullFidelity              = PropTag(PtBoolean, 0x10F2)
	PrAttrHidden                       = PropTag(PtBoolean, 0x10F4)
	PrAttrSystem                       = PropTag(PtBoolean, 0x10F5)
	PrAttrReadonly                     = PropTag(PtBoolean, 0x10F6)
	PrRead                             = PropTag(PtBoolean, 0x0E69)
	PrAdminSecurityDescriptor          = PropTag(PtBinary, 0x3d21)
	PrWin32SecurityDescriptor          = PropTag(PtBinary, 0x3d22)
	PrNonWin32Acl                      = PropTag(PtBoolean, 0x3d23)
	PrItemLevelACL                     = PropTag(PtBoolean, 0x3d24)
	PrDavTransferSecurityDescriptor    = PropTag(PtBinary, 0x0E84)
	PrNtSecurityDescriptorAsXML        = PropTag(PtString, 0x0E6A)
	PrAdminSecurityDescriptorAsXML     = PropTag(PtString, 0x0E6B)
	PrOwaURL                           = PropTag(PtString8, 0x10F1)
	PrSynceventSuppressGUID            = PropTag(PtBinary, 0x3880)
	PrLockBranchID                     = PropTag(PtI8, 0x3800)
	PrLockResourceFid                  = PropTag(PtI8, 0x3801)
	PrLockResourceDid                  = PropTag(PtI8, 0x3802)
	PrLockResourceVid                  = PropTag(PtI8, 0x3803)
	PrLockEnlistmentContext            = PropTag(PtBinary, 0x3804)
	PrLockType                         = PropTag(PtShort, 0x3805)
	PrLockScope                        = PropTag(PtShort, 0x3806)
	PrLockTransientID                  = PropTag(PtBinary, 0x3807)
	PrLockDepth                        = PropTag(PtLong, 0x3808)
	PrLockTimeout                      = PropTag(PtLong, 0x3809)
	PrLockExpiryTime                   = PropTag(PtSystime, 0x380a)
	PrLockGlid                         = PropTag(PtBinary, 0x380b)
	PrLockNullURLW                     = PropTag(PtUnicode, 0x380c)
	PrAntivirusVendor                  = PropTag(PtString8, 0x0E85)
	PrAntivirusVersion                 = PropTag(PtLong, 0x0E86)
	PrAntivirusScanStatus              = PropTag(PtLong, 0x0E87)
	PrAntivirusScanInfo                = PropTag(PtString8, 0x0E88)
	PrAddrTo                           = PropTag(PtString, 0x0E97)
	PrAddrCc                           = PropTag(PtString, 0x0E98)
	PrUserPassword                     = PropTag(PtString, 0x0E89)
	PrFirstName                        = PropTag(PtString, 0x802D)
	PrLastName                         = PropTag(PtString, 0x802C)
	PrProvider                         = PropTag(PtBinary, 0x3434)
	PrExchange                         = PropTag(PtString, 0x802A)
)

// SPropTagArray struct
type SPropTagArray struct {
	CValues    int
	AulPropTag []int
}

// NewSPropTagArray func
func NewSPropTagArray(aulPropTag []int) *SPropTagArray {
	return &SPropTagArray{CValues: len(aulPropTag), AulPropTag: aulPropTag}
}

// Set setter function
func (sPropTagArray *SPropTagArray) Set(aulPropTag []int) {
	sPropTagArray.AulPropTag = aulPropTag
	sPropTagArray.CValues = len(aulPropTag)
}

// LPSPropsValue props array
type LPSPropsValue []SPropValue

// SPropValue props value maps.
type SPropValue map[int]interface{}

// LPSProps interface
type LPSProps interface {
	GetProps() SPropValue
	Get(int) interface{}
	Set(int, interface{})
	ToBSON() primitive.D
	ToJSON() string
	ToJSONIndent() string
	ToSPropValue(d primitive.D) SPropValue
	TranspileToSPropValue(j io.Reader) SPropValue
}

// NewSProps func
func NewSProps() LPSProps {
	return make(SPropValue)
}

// GetProps func
func (sp SPropValue) GetProps() SPropValue {
	return sp
}

// Get func
func (sp SPropValue) Get(key int) interface{} {
	return sp[key]
}

// Set func
func (sp SPropValue) Set(tag int, value interface{}) {
	switch PropType(tag) {
	case PtString8, PtString:
		sp[tag] = cast.ToString(value)
		break
	case PtLong:
		sp[tag] = cast.ToInt(value)
		break
	case PtI8:
		sp[tag] = cast.ToInt8(value)
		break
	case PtSystime:
		// TODO: error checking
		sp[tag] = cast.ToTime(value).Unix()
		break
	case PtBoolean:
		sp[tag] = cast.ToBool(value)
		break
	case PtBinary:
		// TODO:
		// log.Println("value:-", string(value.([]uint8)))
		// log.Println("called")
		// buf := new(bytes.Buffer)
		// log.Println(value)
		// if err := binary.Write(buf, binary.LittleEndian, value); err != nil {
		// 	log.Println("err:-", err)
		// 	return
		// }
		// log.Println("buf:-", buf)
		//sp[tag] = value
		break
	}
}

// ToBSON func
func (sp SPropValue) ToBSON() (props primitive.D) {
	for k, v := range sp {
		props = append(props, primitive.E{Key: cast.ToString(k), Value: v})
	}
	return
}

// ToJSON func
func (sp SPropValue) ToJSON() string {
	var result, err = json.Marshal(sp)
	if err != nil {
		log.Println("ERROR:-", err)
	}
	return string(result)
}

// ToJSONIndent func
func (sp SPropValue) ToJSONIndent() string {
	var result, err = json.MarshalIndent(sp, "", "    ")
	if err != nil {
		log.Println("ERROR:-", err)
	}
	return string(result)
}

// ToSPropValue func
func (sp SPropValue) ToSPropValue(props primitive.D) SPropValue {
	for t, v := range props.Map() {
		if t == "_id" {
			continue
		}
		sp.Set(cast.ToInt(t), v)
	}
	return sp
}

// TranspileToSPropValue func
func (sp SPropValue) TranspileToSPropValue(j io.Reader) SPropValue {
	decoder := json.NewDecoder(j)
	if err := decoder.Decode(&sp); err == nil {
		return sp
	}
	return nil
}
