package model

import (
	"encoding/json"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	// NSE Exchange id
	NSE = iota

	// BSE Exchange id
	BSE
)

// Ticker struct
type Ticker struct {
	ID           primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Name         string             `bson:"name,omitempty" json:"name,omitempty"`
	ExchangeType int                `bson:"exchange_type,omitempty" json:"exchange_type,omitempty"`
}

// ToJSON convert a User to a json string
func (t *Ticker) ToJSON() string {
	b, err := json.Marshal(t)
	if err != nil {
		return ""
	}
	return string(b)
}
