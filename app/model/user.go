package model

import (
	"github.com/pborman/uuid"
	"io"
	"time"
)

type IUser interface {
	Model
}

type EmployeeOrgData struct {
	CostCenter string `json:"cost_center,omitempty"`
	Division   string `json:"division,omitempty"`
}

type MailboxSettings struct {
	ArchiveFolder           string `json:"archiveFolder,omitempty"`
	AutomaticRepliesSetting struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"automaticRepliesSetting,omitempty"`
	DateFormat                            string `json:"dateFormat,omitempty"`
	DelegateMeetingMessageDeliveryOptions string `json:"delegateMeetingMessageDeliveryOptions,omitempty"`
	Language                              struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"language,omitempty"`
	TimeFormat   string `json:"timeFormat,omitempty"`
	TimeZone     string `json:"timeZone,omitempty"`
	WorkingHours struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"workingHours,omitempty"`
}

type User struct {
	AboutMe          string `json:"aboutMe,omitempty"`
	AccountEnabled   bool   `json:"accountEnabled,omitempty"`
	AgeGroup         string `json:"ageGroup,omitempty"`
	AssignedLicenses []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"assignedLicenses,omitempty"`
	AssignedPlans []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"assignedPlans,omitempty"`
	Birthday                string           `json:"birthday,omitempty"`
	BusinessPhones          []string         `json:"businessPhones,omitempty"`
	City                    string           `json:"city,omitempty"`
	CompanyName             string           `json:"companyName,omitempty"`
	ConsentProvidedForMinor string           `json:"consentProvidedForMinor,omitempty"`
	Country                 string           `json:"country,omitempty"`
	CreatedDateTime         string           `json:"createdDateTime,omitempty"`
	CreationType            string           `json:"creationType,omitempty"`
	Department              string           `json:"department,omitempty"`
	DisplayName             string           `json:"displayName,omitempty"`
	EmployeeHireDate        time.Time        `json:"employeeHireDate,omitempty"`
	EmployeeID              string           `json:"employeeId,omitempty"`
	EmployeeOrgData         *EmployeeOrgData `json:"employeeOrgData,omitempty"`
	EmployeeType            string           `json:"employeeType,omitempty"`
	FaxNumber               string           `json:"faxNumber,omitempty"`
	GivenName               string           `json:"givenName,omitempty"`
	HireDate                string           `json:"hireDate,omitempty"`
	ID                      string           `json:"id,omitempty"`
	Identities              []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"identities,omitempty"`
	ImAddresses                 []string `json:"imAddresses,omitempty"`
	Interests                   []string `json:"interests,omitempty"`
	IsResourceAccount           bool     `json:"isResourceAccount,omitempty"`
	JobTitle                    string   `json:"jobTitle,omitempty"`
	LegalAgeGroupClassification string   `json:"legalAgeGroupClassification,omitempty"`
	LicenseAssignmentStates     []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"licenseAssignmentStates,omitempty"`
	LastPasswordChangeDateTime    string           `json:"lastPasswordChangeDateTime,omitempty"`
	Mail                          string           `json:"mail,omitempty"`
	MailboxSettings               *MailboxSettings `json:"mailboxSettings,omitempty"`
	MailNickname                  string           `json:"mailNickname,omitempty"`
	MobilePhone                   string           `json:"mobilePhone,omitempty"`
	MySite                        string           `json:"mySite,omitempty"`
	OfficeLocation                string           `json:"officeLocation,omitempty"`
	OnPremisesDistinguishedName   string           `json:"onPremisesDistinguishedName,omitempty"`
	OnPremisesDomainName          string           `json:"onPremisesDomainName,omitempty"`
	OnPremisesExtensionAttributes struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"onPremisesExtensionAttributes,omitempty"`
	OnPremisesImmutableID        string `json:"onPremisesImmutableId,omitempty"`
	OnPremisesLastSyncDateTime   string `json:"onPremisesLastSyncDateTime,omitempty"`
	OnPremisesProvisioningErrors []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"onPremisesProvisioningErrors,omitempty"`
	OnPremisesSamAccountName     string   `json:"onPremisesSamAccountName,omitempty"`
	OnPremisesSecurityIdentifier string   `json:"onPremisesSecurityIdentifier,omitempty"`
	OnPremisesSyncEnabled        bool     `json:"onPremisesSyncEnabled,omitempty"`
	OnPremisesUserPrincipalName  string   `json:"onPremisesUserPrincipalName,omitempty"`
	OtherMails                   []string `json:"otherMails,omitempty"`
	PasswordPolicies             string   `json:"passwordPolicies,omitempty"`
	PasswordProfile              struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"passwordProfile,omitempty"`
	PastProjects      []string `json:"pastProjects,omitempty"`
	PostalCode        string   `json:"postalCode,omitempty"`
	PreferredLanguage string   `json:"preferredLanguage,omitempty"`
	PreferredName     string   `json:"preferredName,omitempty"`
	ProvisionedPlans  []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"provisionedPlans,omitempty"`
	ProxyAddresses                  []string `json:"proxyAddresses,omitempty"`
	Responsibilities                []string `json:"responsibilities,omitempty"`
	Schools                         []string `json:"schools,omitempty"`
	ShowInAddressList               bool     `json:"showInAddressList,omitempty"`
	SignInSessionsValidFromDateTime string   `json:"signInSessionsValidFromDateTime,omitempty"`
	Skills                          []string `json:"skills,omitempty"`
	State                           string   `json:"state,omitempty"`
	StreetAddress                   string   `json:"streetAddress,omitempty"`
	Surname                         string   `json:"surname,omitempty"`
	UsageLocation                   string   `json:"usageLocation,omitempty"`
	UserPrincipalName               string   `json:"userPrincipalName,omitempty"`
	UserType                        string   `json:"userType,omitempty"`
	Calendar                        struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"calendar,omitempty"`
	CalendarGroups []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"calendarGroups,omitempty"`
	CalendarView []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"calendarView,omitempty"`
	Calendars []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"calendars,omitempty"`
	Contacts []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"contacts,omitempty"`
	ContactFolders []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"contactFolders,omitempty"`
	CreatedObjects []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"createdObjects,omitempty"`
	DirectReports []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"directReports,omitempty"`
	Drive struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"drive,omitempty"`
	Drives []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"drives,omitempty"`
	Events []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"events,omitempty"`
	InferenceClassification struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"inferenceClassification,omitempty"`
	MailFolders []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"mailFolders,omitempty"`
	Manager struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"manager,omitempty"`
	MemberOf []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"memberOf,omitempty"`
	Messages []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"messages,omitempty"`
	Outlook struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"outlook,omitempty"`
	OwnedDevices []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"ownedDevices,omitempty"`
	OwnedObjects []struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"ownedObjects,omitempty"`
	Photo struct {
		OdataType string `json:"@odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"photo,omitempty"`
	RegisteredDevices []struct {
		OdataType string `json:"@Odata.type,omitempty" bson:"@Odata.type,omitempty"`
	} `json:"registeredDevices,omitempty"`

	Extension []*Extension `json:"extensions,omitempty"`
	modelImpl
}

func (u *User) FromJson(data io.Reader) (interface{}, error) {
	model, err := u.modelImpl.FromJson(data, u)
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (u *User) PreSave() {

	if len(u.ID) == 0 {
		u.ID = uuid.NewUUID().String()
	}

	if len(u.Extension) != 0 {
		for _, extension := range u.Extension {
			extension.Create()
		}
	}
}
