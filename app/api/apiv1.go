package api

import (
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/snehal1112/stock/app"
	"strings"
)

type router = map[string]*mux.Router

// API struct
type API struct {
	App        *app.App
	BaseRoutes router
	basePath   string
	rootRouter *mux.Router
	logger     logrus.FieldLogger
}

// Init func
func Init(options ...Options) *API {
	api := &API{
		BaseRoutes: make(router),
	}

	for _, option := range options {
		option(api)
	}

	api.logger.WithField("basePath", api.basePath).Infoln("Api configured with")

	api.BaseRoutes["ApiRoot"] = api.rootRouter.PathPrefix(api.basePath).Subrouter()
	baseRoutes := api.BaseRoutes["ApiRoot"]

	api.BaseRoutes["Stock"] = baseRoutes.PathPrefix("/stock").Subrouter()
	api.BaseRoutes["Users"] = baseRoutes.PathPrefix("/users").Subrouter()
	api.BaseRoutes["Contact"] = baseRoutes.PathPrefix("/contacts").Subrouter()
	api.BaseRoutes["ContactFolder"] = baseRoutes.PathPrefix("/contactFolders").Subrouter()

	api.InitStock(api.BaseRoutes["Stock"])
	api.InitUsers(api.BaseRoutes["Users"])
	api.InitContacts(api.BaseRoutes["Contact"])
	api.InitContactFolder(api.BaseRoutes["ContactFolder"])

	var apiNames []string
	for s, _ := range api.BaseRoutes {
		if s != "ApiRoot" {
			apiNames = append(apiNames, s)
		}
	}

	logrus.WithField("api", strings.Join(apiNames,",")).Infoln("Initialized api")
	return api
}
