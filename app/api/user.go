package api

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/snehal1112/stock/model"
)

// InitUsers func
func (api *API) InitUsers(user *mux.Router) {
	user.Handle("", api.APISessionRequired(createUser)).Methods("POST")
	user.Handle("", api.APISessionRequired(getcreateUser)).Methods("GET")
	user.Handle("/{id}/extensions", api.APISessionRequired(createExtensions)).Methods("POST")
	user.Handle("/{userPrincipalName}/contactFolders/{contactFolderId}/contacts", api.APISessionRequired(createContact)).Methods("POST")
	user.Handle("/{userPrincipalName}/contacts", api.APISessionRequired(createContact)).Methods("POST")
}

func getDefaultModel() *model.User {
	return &model.User{}
}

func getcreateUser(c *Context, w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("sddddd"))
}

func createUser(c *Context, w http.ResponseWriter, r *http.Request) {
	userModel, modelErr := getDefaultModel().FromJson(r.Body)
	if modelErr != nil {
		c.Err = model.NewAppError("api.createUser", "json.conversation.app_error", nil, "id="+modelErr.Error(), http.StatusBadRequest)
		return
	}

	props, createUserErr := c.App.CreateUser(userModel.(*model.User))
	if createUserErr != nil {
		c.Err = createUserErr
		return
	}

	w.WriteHeader(http.StatusCreated)
	result, jsonErr := props.ToJson(props)
	if jsonErr != nil {
		c.Err = model.NewAppError("api.createUser", "json.conversation.app_error", nil, "id="+modelErr.Error(), http.StatusBadRequest)
		return
	}
	w.Write(result)
}

func createExtensions(c *Context, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(vars["id"]))
}
