package api

import (
	"github.com/gorilla/mux"
)

// InitContactFolder func
func (api *API) InitContactFolder(contacts *mux.Router) {
	contacts.Handle("{contactFolderId}/contacts", api.APISessionRequired(createContact)).Methods("POST")

}
