package api

import (
	"crypto/tls"
	"crypto/x509"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/snehal1112/stock/model"

	"github.com/gorilla/mux"
)

// InitStock func
func (api *API) InitStock(stock *mux.Router) {
	stock.Handle("", api.APIHandler(createTicker)).Methods("GET")
}

func callContactService() {
	caCert, err := ioutil.ReadFile("/data/stock/tls/root_ca.crt")
	log.Println("err:-", err)
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	cert, err := tls.LoadX509KeyPair("/data/stock/tls/contact.crt", "/data/stock/tls/contact.key")
	log.Println("LoadX509KeyPair err:-", err)
	client := &http.Client{
		Transport: &http.Transport{
			ResponseHeaderTimeout: 10 * time.Second,
			ForceAttemptHTTP2:     true,
			TLSClientConfig: &tls.Config{
				RootCAs:      caCertPool,
				Certificates: []tls.Certificate{cert},
			},
		},
	}

	log.Println("before call client.Get")
	r, err := client.Get("https://stock.lxd/api/v1/discovery")
	log.Println("err:-", err)
	log.Println("r:-", r)

	body, _ := io.ReadAll(r.Body)
	log.Println("body:----", string(body))
}

func createTicker(c *Context, w http.ResponseWriter, r *http.Request) {
	callContactService()
	props := model.NewSProps()
	ticker := props.TranspileToSPropValue(r.Body)

	propss, err := c.App.CreateTicker(ticker)
	if err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(propss.ToJSON()))
}
