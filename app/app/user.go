package app

import (
	"github.com/snehal1112/stock/model"
	"github.com/snehal1112/stock/utils"
)

const tableUser = "users"

// CreateUser func
func (app *App) CreateUser(ticker *model.User) (*model.User, *model.AppError) {
	result := <-app.store.User().Create(tableUser, ticker)
	if result.Err != nil {
		app.logger.Errorln(utils.T("store.message_store.create.ipm_subtree.app_error"))
		return nil, result.Err
	}
	return result.Data.(*model.User), nil
}
