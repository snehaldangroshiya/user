package app

import (
	"github.com/snehal1112/stock/model"
	"github.com/snehal1112/stock/utils"
)

const tableTicker = "ticker"

// CreateTicker func
func (app *App) CreateTicker(ticker model.SPropValue) (model.SPropValue, *model.AppError) {
	result := <-app.store.Stock().Create(tableTicker, ticker)
	if result.Err != nil {
		app.logger.Errorln(utils.T("store.message_store.create.ipm_subtree.app_error"))
		return nil, result.Err
	}
	return result.Data.(model.SPropValue), nil
}
