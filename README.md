# User

Prototype for the service mesh implementation


# Renew certificate
### If certificate is expired then you can't renew u need to regenerate
```
TOKEN=$(step ca token $(hostname) --ca-url  https://camanager.local)

step ca certificate $(hostname) server.crt server.key --token $TOKEN --ca-url https://camanager.local --root $(step path)/certs/root_ca.crt --not-after "$(date --date "next year" -Iseconds)"

✔ CA: https://camanager.local
✔ Would you like to overwrite server.crt [y/n]: Y
✔ Would you like to overwrite server.key [y/n]: Y
✔ Certificate: server.crt
✔ Private Key: server.key

```
### If certificate is not expired 
```
step ca renew --ca-url https://camanager.local --root $(step path)/certs/root_ca.crt server.crt server.key

```

